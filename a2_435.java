import java.util.Scanner;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.text.DecimalFormat;

public class a2_435
{
	DecimalFormat maeFormat = new DecimalFormat("#.####");
	DecimalFormat imputedFormat = new DecimalFormat("#.#####");
	public static void main(String[] args)
	{
		a2_435 program = new a2_435();
		program.run();
	}

	public void run()
	{
		String complete_file = "dataset_complete.csv";
		String missing01_file = "dataset_missing01.csv";
		String missing10_file = "dataset_missing10.csv";
		String line = "";
		String fields[];
		int i, j, k;
		ArrayList<Entry> arrListComplete = new ArrayList<Entry>();
		Entry[] completedArr;
		ArrayList<Entry> missing01Arr = new ArrayList<Entry>();
		ArrayList<Entry> incomplete01 = new ArrayList<Entry>();
		ArrayList<Entry> missing10Arr = new ArrayList<Entry>();
		ArrayList<Entry> incomplete10 = new ArrayList<Entry>();
		try 
		{
			i = 0;
			Entry entry = new Entry();
			BufferedReader reader = new BufferedReader(new FileReader(complete_file));
			line = reader.readLine();
			while((line = reader.readLine()) != null)
			{
				entry.index = i;
				fields = line.split(",");
				for(j = 0; j < 13; j++)
				{
					if(fields[j].equals("?"))
					{
						entry.feature[j] = Double.NaN;
					}
					else
					{
						entry.feature[j] = Double.parseDouble(fields[j]);
					}
				}
				if(fields[j].toLowerCase().equals("y"))
				{
					entry.reacts = true;
				}
				else
				{
					entry.reacts = false;
				}
				arrListComplete.add(entry);
				entry = new Entry();
				i++;
			}
			completedArr = new Entry[arrListComplete.size()];
			//I am moving this into an array to reduce complexity of
			//computing error later. I will only have one copy of the 
			//complete data set after this as I set the arraylist to null.
			for(Entry e : arrListComplete)
			{
				completedArr[e.index] = e;
			}
			arrListComplete = null;
			reader = new BufferedReader(new FileReader(missing01_file));
			i = 0;
			entry = new Entry();
			boolean incmp = false;
			entry.index = i;
			line = reader.readLine();
			while((line = reader.readLine()) != null)
			{
				entry.index = i;
				fields = line.split(",");
				for(j = 0; j < 13; j++)
				{
					if(fields[j].equals("?"))
					{
						incmp = true;
						entry.feature[j] = Double.NaN;
					}
					else
					{
						entry.feature[j] = Double.parseDouble(fields[j]);
					}
				}
				if(fields[j].toLowerCase().equals("y"))
				{
					entry.reacts = true;
				}
				else if(fields[j].toLowerCase().equals("f"))
				{
					entry.reacts = false;
				}
				else
				{
					incmp = true;
				}
				if(incmp)
				{
					incomplete01.add(entry);
					incmp = false;
				}
				else
				{
					missing01Arr.add(entry);
				}
				entry = new Entry();
				i++;
			}
			System.out.println("MAE_01_mean = " + maeFormat.format(meanImputationUnconditional(completedArr, missing01Arr, incomplete01, "01")));
			System.out.println("MAE_01_mean_conditional = " + maeFormat.format(meanImputationConditional(completedArr, missing01Arr, incomplete01, "01")));
			System.out.println("MAE_01_hd = " + maeFormat.format(hotDeckImputationUnconditional(completedArr, missing01Arr, incomplete01, "01")));
			System.out.println("MAE_01_hd_conditional = " + maeFormat.format(hotDeckImputationConditional(completedArr, missing01Arr, incomplete01, "01")));
			reader = new BufferedReader(new FileReader(missing10_file));
			i = 0;
			entry = new Entry();
			incmp = false;
			entry.index = i;
			line = reader.readLine();
			while((line = reader.readLine()) != null)
			{
				entry.index = i;
				fields = line.split(",");
				for(j = 0; j < 13; j++)
				{
					if(fields[j].equals("?"))
					{
						incmp = true;
						entry.feature[j] = Double.NaN;
					}
					else
					{
						entry.feature[j] = Double.parseDouble(fields[j]);
					}
				}
				if(fields[j].toLowerCase().equals("y"))
				{
					entry.reacts = true;
				}
				else if(fields[j].toLowerCase().equals("f"))
				{
					entry.reacts = false;
				}
				else
				{
					incmp = true;
				}
				if(incmp)
				{
					incomplete10.add(entry);
					incmp = false;
				}
				else
				{
					missing10Arr.add(entry);
				}
				entry = new Entry();
				i++;

			}
			System.out.println("MAE_10_mean = " + maeFormat.format(meanImputationUnconditional(completedArr, missing10Arr, incomplete10, "10")));
			System.out.println("MAE_10_mean_conditional = " + maeFormat.format(meanImputationConditional(completedArr, missing10Arr, incomplete10, "10")));
			System.out.println("MAE_10_hd = " + maeFormat.format(hotDeckImputationUnconditional(completedArr, missing10Arr, incomplete10, "10")));
			System.out.println("MAE_10_hd_conditional = " + maeFormat.format(hotDeckImputationConditional(completedArr, missing10Arr, incomplete10, "10")));

		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

	public void serialize(String method, Entry[] values)
	{
		String filename = "V00762285_missing" + method + ".csv";
		try
		{
			PrintWriter w = new PrintWriter(filename);
			for(int i = 0; i < values.length; i++)
			{
				w.println(values[i]);
			}
			w.close();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}
	
	public double hotDeckImputationConditional(Entry[] complete, ArrayList<Entry> full, ArrayList<Entry> missingOrig, String missingString)
	{
		double minDist = Double.MAX_VALUE;
		Entry currMin = null;
		ArrayList<Entry> missing = new ArrayList<Entry>();
		double mae = 0;
		int errors = 0;
		for(Entry e : missingOrig)
		{
			missing.add(new Entry(e));
		}
		for(Entry e : missing)
		{
			for(Entry c : full)
			{
				if(e.dist(c) < minDist && e.reacts == c.reacts)
				{
					currMin = new Entry(c);
					minDist = e.dist(c);
				}
			}
			for(Entry c : missingOrig)
			{
				if(e.dist(c) < minDist && e.reacts == c.reacts)
				{
					boolean cand = true;
					for(int i = 0; i < 13; i++)
					{
						if(Double.isNaN(e.feature[i]) && Double.isNaN(c.feature[i]))cand = false;
					}
					if(cand)
					{
						currMin = new Entry(c);
						minDist = e.dist(c);
					}
				}
			}
			if(currMin != null)
			{
				for(int i = 0; i < 13; i++)
				{
					if(Double.isNaN(e.feature[i]))
					{
						e.feature[i] = currMin.feature[i];
						errors++;
						mae += Math.abs(e.feature[i] - complete[e.index].feature[i]);
					}
				}
			}
			currMin = null;
			minDist = Double.MAX_VALUE;
		}
		Entry[] imputed = new Entry[complete.length];
		String str = missingString + "_imputed_hd_conditional";
		for(Entry e : full)
		{
			imputed[e.index] = new Entry(e);
		}
		for(Entry e : missing)
		{
			imputed[e.index] = new Entry(e);
		}
		serialize(str, imputed);
		return mae / errors;
	}

	public double hotDeckImputationUnconditional(Entry[] complete, ArrayList<Entry> full, ArrayList<Entry> missingOrig, String missingString)
	{
		double minDist = Double.MAX_VALUE;
		Entry currMin = null;
		ArrayList<Entry> missing = new ArrayList<Entry>();
		double mae = 0;
		int errors = 0;
		for(Entry e : missingOrig)
		{
			missing.add(new Entry(e));
		}
		for(Entry e : missing)
		{
			for(Entry c : full)
			{
				if(e.dist(c) < minDist)
				{
					currMin = new Entry(c);
					minDist = e.dist(c);
				}
			}
			for(Entry c : missingOrig)
			{
				if(e.dist(c) < minDist)
				{
					boolean cand = true;
					for(int i = 0; i < 13; i++)
					{
						if(Double.isNaN(e.feature[i]) && Double.isNaN(c.feature[i]))cand = false;
					}
					if(cand)
					{
						currMin = new Entry(c);
						minDist = e.dist(c);
					}
				}
			}
			if(currMin != null)
			{
				for(int i = 0; i < 13; i++)
				{
					if(Double.isNaN(e.feature[i]))
					{
						e.feature[i] = currMin.feature[i];
						errors++;
						mae += Math.abs(e.feature[i] - complete[e.index].feature[i]);
					}
				}
			}
			currMin = null;
			minDist = Double.MAX_VALUE;
		}
		Entry[] imputed = new Entry[complete.length];
		String str = missingString + "_imputed_hd";
		for(Entry e : full)
		{
			imputed[e.index] = new Entry(e);
		}
		for(Entry e : missing)
		{
			imputed[e.index] = new Entry(e);
		}
		serialize(str, imputed);

		return mae / errors;
	}

	public double meanImputationConditional(Entry[] complete, ArrayList<Entry> full, ArrayList<Entry> missingOrig, String missingString)
	{
		int[] totalcountr = new int[13];
		int[] totalcountnr = new int[13];
		double[] meansr = new double[13];
		double[] meansnr = new double[13];
		double mae = 0;
		int errors = 0;
		ArrayList<Entry> missing = new ArrayList<Entry>();
		for(Entry e : missingOrig)
		{
			missing.add(new Entry(e));
		}
		for(Entry e : full)
		{
			for(int i = 0; i < 13; i++)
			{
				if(e.reacts)
				{
					meansr[i] += e.feature[i];
					totalcountr[i]++;
				}
				else
				{
					meansnr[i] += e.feature[i];
					totalcountnr[i]++;
				}
			}
		}
		for(Entry e : missing)
		{
			for(int i = 0; i < 13; i++)
			{
				if(!Double.isNaN(e.feature[i]))
				{
					if(e.reacts)
					{
						meansr[i] += e.feature[i];
						totalcountr[i]++;
					}
					else
					{
						meansnr[i] += e.feature[i];
						totalcountnr[i]++;
					}
				}
			}
		}
		for(int i = 0; i < 13; i++)
		{
			meansr[i] /= totalcountr[i];
			meansnr[i] /= totalcountnr[i];
		}
		for(Entry e : missing)
		{
			for(int i = 0; i < 13; i++)
			{
				if(Double.isNaN(e.feature[i]))
				{
					if(e.reacts)
					{
						e.feature[i] = meansr[i];
					}
					else
					{
						e.feature[i] = meansnr[i];
					}
					if(e.feature[i] != complete[e.index].feature[i])
					{
						errors++;
						mae += Math.abs(e.feature[i] - complete[e.index].feature[i]);
					}
				}
			}
		}
		Entry[] imputed = new Entry[complete.length];
		String str = missingString + "_imputed_mean_conditional";
		for(Entry e : full)
		{
			imputed[e.index] = new Entry(e);
		}
		for(Entry e : missing)
		{
			imputed[e.index] = new Entry(e);
		}
		serialize(str, imputed);

		return mae / errors;
	}


	public double meanImputationUnconditional(Entry[] complete, ArrayList<Entry> full, ArrayList<Entry> missingOrig, String missingString)
	{
		int[] totalcount = new int[13];
		double[] means = new double[13];
		double mae = 0;
		int errors = 0;
		ArrayList<Entry> missing = new ArrayList<Entry>();
		for(Entry e : missingOrig)
		{
			missing.add(new Entry(e));
		}
		for(Entry e : full)
		{
			for(int i = 0; i < 13; i++)
			{
				means[i] += e.feature[i];
				totalcount[i]++;
			}
		}
		for(Entry e : missing)
		{
			for(int i = 0; i < 13; i++)
			{
				if(!Double.isNaN(e.feature[i]))
				{
					means[i] += e.feature[i];
					totalcount[i]++;
				}
			}
		}
		for(int i = 0; i < 13; i++)
		{
			means[i] /= totalcount[i];
		}
		for(Entry e : missing)
		{
			for(int i = 0; i < 13; i++)
			{
				if(Double.isNaN(e.feature[i]))
				{
					e.feature[i] = means[i];
					if(e.feature[i] != complete[e.index].feature[i])
					{
						errors++;
						mae += Math.abs(e.feature[i] - complete[e.index].feature[i]);
					}
				}
			}
		}
		Entry[] imputed = new Entry[complete.length];
		String str = missingString + "_imputed_mean";
		for(Entry e : full)
		{
			imputed[e.index] = new Entry(e);
		}
		for(Entry e : missing)
		{
			imputed[e.index] = new Entry(e);
		}
		serialize(str, imputed);

		return mae / errors;
	}

	public class Entry
	{
		int index;
		double feature[] = new double[13];
		boolean reacts;
		public Entry()
		{
		}

		public Entry(Entry e)
		{
			this.index = e.index;
			for(int i = 0; i < 13; i++)
			{
				this.feature[i] = e.feature[i];
			}
			this.reacts = e.reacts;
		}

		public double dist(Entry e)
		{
			double d = 0;
			for(int i = 0; i < 13; i++)
			{
				if(Double.isNaN(this.feature[i]) || Double.isNaN(e.feature[i]))
				{
					d += 1;
				}
				else
				{
					d += Math.pow(this.feature[i] - e.feature[i], 2);
				}
			}
			return Math.sqrt(d);
		}

		public String toString()
		{
			String str = "";
			for(int i = 0; i < 13; i++)
			{
				str += imputedFormat.format(this.feature[i]) + ",";
			}
			if(this.reacts)
			{
				str += "Y";
			}
			else
			{
				str += "N";
			}
			return str;
		}
	}
}
